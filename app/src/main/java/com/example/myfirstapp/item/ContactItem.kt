package com.example.myfirstapp.item

import android.view.View
import com.example.myfirstapp.R
import com.example.myfirstapp.databinding.ItemContactBinding
import com.example.myfirstapp.model.Contact
import com.xwray.groupie.viewbinding.BindableItem

class ContactItem(var contact: Contact) : BindableItem<ItemContactBinding>(contact.hashCode().toLong()) {

    override fun bind(viewBinding: ItemContactBinding, position: Int) {
        viewBinding.data = contact
    }

    override fun getLayout(): Int = R.layout.item_contact

    override fun initializeViewBinding(view: View): ItemContactBinding = ItemContactBinding.bind(view)

    fun updateData(contact: Contact) {
        this.contact = contact
        notifyChanged()
    }
}