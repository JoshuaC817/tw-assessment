package com.example.myfirstapp.model

import android.os.Parcelable
import android.text.TextUtils
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Contact(
        var id: String,
        var firstName: String = "",
        var lastName: String = "",
        var email: String? = "",
        var phone: String? = ""
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        if (other is Contact)
            return TextUtils.equals(id, other.id)
        return false
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    fun getFullName() = "$firstName $lastName"

    fun update(data: Contact) {
        firstName = data.firstName
        lastName = data.lastName
        email = data.email
        phone = data.phone
    }


}