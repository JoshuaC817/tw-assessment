package com.example.myfirstapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myfirstapp.R
import com.example.myfirstapp.databinding.ActivityMainBinding
import com.example.myfirstapp.databinding.DialogSearchBinding
import com.example.myfirstapp.item.ContactItem
import com.example.myfirstapp.model.Contact
import com.example.myfirstapp.ui.detail.DetailActivity
import com.example.myfirstapp.util.readAssetFile
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.Section

class MainActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE = 2324
        private const val CREATE_REQUEST_CODE = 2325
    }

    private var mBinding: ActivityMainBinding? = null
    private var mViewModel: MainViewModel? = null

    private val mSection = Section()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mViewModel?.displayListLiveData?.observe(this, mListObserver)

        initView()
        getOriList()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            val contact = data?.getParcelableExtra<Contact>(DetailActivity.DATA) ?: return
            if (requestCode == REQUEST_CODE) {
                Toast.makeText(this, R.string.contact_update, Toast.LENGTH_LONG).show()
                mViewModel?.update(contact)
            } else if (requestCode == CREATE_REQUEST_CODE) {
                Toast.makeText(this, R.string.new_contact_created, Toast.LENGTH_LONG).show()
                mViewModel?.add(contact)
            }
        }
    }

    private fun initView() {
        mBinding?.swipeRefresh?.setOnRefreshListener { getOriList() }

        val adapter = GroupieAdapter()
        adapter.add(mSection)
        mBinding?.recyclerView?.adapter = adapter

        adapter.setOnItemClickListener { item, view ->
            if (item is ContactItem) {
                DetailActivity.startForResult(this, item.contact, REQUEST_CODE)
            }
        }

        mBinding?.ivAdd?.setOnClickListener {
            DetailActivity.startForResult(this, null, CREATE_REQUEST_CODE)
        }
        mBinding?.ivSearch?.setOnClickListener { showSearch() }
    }

    private fun getOriList() {
        mViewModel?.getListFromJson(assets.readAssetFile("data.json"))
    }

    private val mListObserver = Observer<ArrayList<Contact>> { list ->
        mSection.update(list.map { ContactItem(it) })
        mBinding?.swipeRefresh?.isRefreshing = false
    }

    //tried to do search but time is up :(
    private fun showSearch() {
        val binding = DialogSearchBinding.inflate(LayoutInflater.from(this))
        val dialog = AlertDialog.Builder(this).setView(binding.root)
                .create()

        binding.btnSearch.setOnClickListener { dialog.dismiss() }
        binding.btnCancel.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }
}