package com.example.myfirstapp.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myfirstapp.model.Contact
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

class MainViewModel : ViewModel() {

    private val oriListLiveData = arrayListOf<Contact>()
    val displayListLiveData = MutableLiveData<ArrayList<Contact>>()

    fun getListFromJson(data: String) {
        val gson = GsonBuilder().create()
        val list = gson.fromJson<ArrayList<Contact>>(data, object : TypeToken<ArrayList<Contact>>() {}.type)

        oriListLiveData.clear()
        oriListLiveData.addAll(list)

        displayListLiveData.value = list
    }

    fun update(contact: Contact) {
        val list = displayListLiveData.value
        if (list.isNullOrEmpty()) return

        list.find { it.id == contact.id }?.update(contact)
        displayListLiveData.value = list!!
    }

    fun add(contact: Contact) {
        val list = displayListLiveData.value
        if (list.isNullOrEmpty()) return

        list.add(contact)
        displayListLiveData.value = list!!
    }
}