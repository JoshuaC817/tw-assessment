package com.example.myfirstapp.ui.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.myfirstapp.R
import com.example.myfirstapp.databinding.ActivityDetailBinding
import com.example.myfirstapp.model.Contact
import com.example.myfirstapp.util.hideKeyboard
import java.util.*

class DetailActivity : AppCompatActivity() {

    companion object {

        const val DATA = "data"

        fun startForResult(activity: Activity, data: Contact?, requestCode: Int) {
            val intent = Intent(activity, DetailActivity::class.java)
            data?.let { intent.putExtra(DATA, it) }
            activity.startActivityForResult(intent, requestCode)
        }
    }

    private var mBinding: ActivityDetailBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail)

        val data = intent?.getParcelableExtra<Contact>(DATA)
        data?.let {
            mBinding?.data = it
        }

        initView()
    }

    private fun initView() {
        mBinding?.tvCancel?.setOnClickListener { finish() }
        mBinding?.tvSave?.setOnClickListener { onSaveClicked() }
    }

    private fun onSaveClicked() {
        val firstName = mBinding?.etFirstName?.text.toString().trim()
        val lastName = mBinding?.etLastName?.text.toString().trim()
        val email = mBinding?.etEmail?.text.toString().trim()
        val phone = mBinding?.etPhone?.text.toString().trim()

        if (firstName.isNullOrBlank()) {
            mBinding?.etFirstName?.error = getString(R.string.error_blank)
            mBinding?.etFirstName?.requestFocus()
            return
        }
        mBinding?.etFirstName?.error = null

        if (lastName.isNullOrBlank()) {
            mBinding?.etLastName?.error = getString(R.string.error_blank)
            mBinding?.etLastName?.requestFocus()
            return
        }
        mBinding?.etLastName?.error = null

        mBinding?.etLastName?.hideKeyboard()

        val data = intent?.getParcelableExtra(DATA)
                ?: Contact(UUID.randomUUID().toString())
        data.let {
            it.firstName = firstName
            it.lastName = lastName
            it.email = email
            it.phone = phone
        }

        val intent = Intent()
        intent.putExtra(DATA, data)
        setResult(RESULT_OK, intent)
        finish()
    }
}