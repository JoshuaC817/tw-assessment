package com.example.myfirstapp.util

import android.app.Activity
import android.content.res.AssetManager
import android.view.View
import android.view.inputmethod.InputMethodManager

fun AssetManager.readAssetFile(fileName: String): String = open(fileName).bufferedReader().use { it.readText() }

fun View?.hideKeyboard() {
    this?.let {
        val inputMethodManager =
                it.context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
    }
}